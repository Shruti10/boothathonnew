function area1()
{
    var t11 : HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
    var t12 : HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
    var t21 : HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    var t22 : HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
    var t31: HTMLInputElement=<HTMLInputElement>document.getElementById("t31");
    var t32: HTMLInputElement=<HTMLInputElement>document.getElementById("t32");

    var ans: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("arans");

    var ax:number = parseFloat(t11.value);
    var ay:number = parseFloat(t12.value);
    var bx:number = parseFloat(t21.value);
    var by:number = parseFloat(t22.value);
    var cx:number = parseFloat(t31.value);
    var cy:number = parseFloat(t32.value);

    var sidea:number = Math.sqrt(Math.pow((bx-ax),2)-Math.pow((by-ay),2));
    var sideb:number = Math.sqrt(Math.pow((cx-bx),2)-Math.pow((cy-by),2));
    var sidec:number = Math.sqrt(Math.pow((ax-cx),2)-Math.pow((ay-cy),2));

    var s:number = (sidea + sideb + sidec)/2;

    var area:number = Math.sqrt(s*(s-sidea)*(s-sideb)*(s-sidec));

    ans.innerHTML="area <br/>"+area.toString();

}