var t1 : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
var t2 : HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
var t3 : HTMLInputElement = <HTMLInputElement>document.getElementById("t3");

function add()
{
    var c:number = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString();
}

function sub()
{
    var c:number = parseInt(t1.value) - parseInt(t2.value);
    t3.value = c.toString();
}

function mul()
{
    var c:number = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}

function div()
{
    var c:number = parseFloat(t1.value) / parseFloat(t2.value);
    t3.value = c.toString();
}

function sin1()
{
    var x:number = parseFloat(t1.value);
    var c:number = Math.sin(Math.sin((x*Math.PI)/180));
    t3.value = c.toString(); 
}

function cos1()
{
    var x:number = parseFloat(t1.value);
    var c:number = Math.cos(Math.sin((x*Math.PI)/180));
    t3.value = c.toString(); 
}

function tan1()
{
    var x:number = parseFloat(t1.value);
    var c:number = Math.tan(Math.sin((x*Math.PI)/180));
    t3.value = c.toString(); 
}

function sqroot()
{
    var x:number = parseFloat(t1.value);
    var c:number = Math.sqrt(x);
    t3.value = c.toString(); 
}

function power()
{
    var x:number = parseFloat(t1.value);
    var y:number = parseFloat(t2.value);
    var c:number = Math.pow(x,y);
    t3.value = c.toString(); 
}