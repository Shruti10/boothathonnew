var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString();
}
function sub() {
    var c = parseInt(t1.value) - parseInt(t2.value);
    t3.value = c.toString();
}
function mul() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}
function div() {
    var c = parseFloat(t1.value) / parseFloat(t2.value);
    t3.value = c.toString();
}
function sin1() {
    var x = parseFloat(t1.value);
    var c = Math.sin(Math.sin((x * Math.PI) / 180));
    t3.value = c.toString();
}
function cos1() {
    var x = parseFloat(t1.value);
    var c = Math.cos(Math.sin((x * Math.PI) / 180));
    t3.value = c.toString();
}
function tan1() {
    var x = parseFloat(t1.value);
    var c = Math.tan(Math.sin((x * Math.PI) / 180));
    t3.value = c.toString();
}
function sqroot() {
    var x = parseFloat(t1.value);
    var c = Math.sqrt(x);
    t3.value = c.toString();
}
function power() {
    var x = parseFloat(t1.value);
    var y = parseFloat(t2.value);
    var c = Math.pow(x, y);
    t3.value = c.toString();
}
//# sourceMappingURL=app1.js.map