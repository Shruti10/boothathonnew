function calculate()
{
    var t1 :HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var ans:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");

    var x:number = parseFloat(t1.value);
    
    var cosval:number = Math.cos((x*Math.PI)/180);

    ans.innerHTML = "value of cos(x) = <br>" + cosval.toString(); 

    var finalans:number = x + cosval;

    ans.innerHTML += "<br><br><br>value of x + cos(x) = <br>" + finalans.toString(); 

}