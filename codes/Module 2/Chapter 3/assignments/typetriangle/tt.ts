function check()
{
    var t1 : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var t2 : HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var t3 : HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
    var ans: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");

    var a:number = parseFloat(t1.value);
    var b:number = parseFloat(t2.value);
    var c:number = parseFloat(t3.value);

    if(a==b && a==c && b==c)
    {
        ans.innerHTML = "<br> Equilateral triangle";
    }    

    else if(a==b || b==c || a==c)
    {
        ans.innerHTML = "<br> Isoceles triangle";
    }
    else if(a!=b && a!=c && b!=c)
    {
        ans.innerHTML = "<br> Scalene triangle";
    }

    if(((a*a)+(b*b))==(c*c))
    ans.innerHTML += "<br>Right angled triangle";
    else
    ans.innerHTML += "<br>Not Right angled triangle";
    

}