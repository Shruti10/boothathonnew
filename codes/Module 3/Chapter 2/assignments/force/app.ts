function forcecos(f1:number,f2:number):number                     //fn to calculate fcos(theta)
{
    var fcos:number = f1 * Math.cos((f2*Math.PI)/180);
    return fcos;
}

function forcesin(f1:number,f2:number):number                       //fn to calculate fsin(theta)
{
    var fsin:number = f1 * Math.sin((f2*Math.PI)/180);
    return fsin;
}

function calc()                                                     //main calculate fn
{
    var t11 : HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
    var t12 : HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
    var t21 : HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    var t22 : HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
        
    var ans: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");
    
    var fm1:number = parseInt(t11.value);
    var ft1:number = parseFloat(t12.value);
    var fm2:number = parseFloat(t21.value);
    var ft2:number = parseFloat(t22.value);
    
    
    var sfx : number = forcecos(fm1,ft1) + forcecos(fm2,ft2);
    var sfy : number = forcesin(fm1,ft1) + forcesin(fm2,ft2);
    
    alert("sum fx=" + sfx);
    alert("sum fy=" + sfy);

    var resf : number = Math.sqrt(Math.pow(sfx,2)+Math.pow(sfy,2));
    var resth : number = Math.atan2(sfy,sfx);
    var resth1 : number = (resth*180)/(Math.PI);

    ans.innerHTML = "<br> Resultant force : " + resf.toString();
    ans.innerHTML += "<br> Resultant angle : " + resth1.toString();
}