function forcecos(f1, f2) {
    var fcos = f1 * Math.cos((f2 * Math.PI) / 180);
    return fcos;
}
function forcesin(f1, f2) {
    var fsin = f1 * Math.sin((f2 * Math.PI) / 180);
    return fsin;
}
function calc() {
    var t11 = document.getElementById("t11");
    var t12 = document.getElementById("t12");
    var t21 = document.getElementById("t21");
    var t22 = document.getElementById("t22");
    var ans = document.getElementById("ans");
    var fm1 = parseInt(t11.value);
    var ft1 = parseFloat(t12.value);
    var fm2 = parseFloat(t21.value);
    var ft2 = parseFloat(t22.value);
    var sfx = forcecos(fm1, ft1) + forcecos(fm2, ft2);
    var sfy = forcesin(fm1, ft1) + forcesin(fm2, ft2);
    alert("sum fx=" + sfx);
    alert("sum fy=" + sfy);
    var resf = Math.sqrt(Math.pow(sfx, 2) + Math.pow(sfy, 2));
    var resth = Math.atan2(sfy, sfx);
    var resth1 = (resth * 180) / (Math.PI);
    ans.innerHTML = "<br> Resultant force : " + resf.toString();
    ans.innerHTML += "<br> Resultant angle : " + resth1.toString();
}
//# sourceMappingURL=app.js.map