function display(r, c, mat, msg) {
    var ans = document.getElementById("ans");
    ans.innerHTML += msg;
    for (var i = 0; i < r; i++) {
        for (var j = 0; j < c; j++) {
            ans.innerHTML += mat[i][j] + '&nbsp';
        }
        ans.innerHTML += '<br>';
    }
}
function mul() {
    var matrix1 = [[10, 22, 31], [14, 2, 14], [1, 12, 13]];
    var matrix2 = [[21, 2, 13], [4, 21, 6], [14, 8, 16]];
    var result = [[], [], []];
    display(3, 3, matrix1, "Matrix A is:<br>");
    display(3, 3, matrix2, "Matrix B is:<br>");
    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            for (var k = 0; k < 3; k++) {
                result[i][j] = Number(matrix1[i][k] * matrix2[k][j]); //multiplication get stored in result matrix
            }
        }
    }
    //to display result 
    display(3, 3, result, "Multiplication of two matrix is:<br>");
}
//# sourceMappingURL=m.js.map