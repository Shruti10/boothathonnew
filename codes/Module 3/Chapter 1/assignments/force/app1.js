function forcecos(f1, f2) {
    var fcos = f1 * Math.cos((f2 * Math.PI) / 180);
    return fcos;
}
function forcesin(f1, f2) {
    var fsin = f1 * Math.sin((f2 * Math.PI) / 180);
    return fsin;
}
function calc() {
    var t11 = document.getElementById("t11");
    var t12 = document.getElementById("t12");
    var t21 = document.getElementById("t21");
    var t22 = document.getElementById("t22");
    var ans = document.getElementById("ans");
    var fm = [];
    var ft = [];
    fm[0] = parseInt(t11.value);
    ft[0] = parseFloat(t12.value);
    fm[1] = parseFloat(t21.value);
    ft[1] = parseFloat(t22.value);
    fm[2] = forcecos(fm[0], ft[0]) + forcecos(fm[1], ft[1]); //fm[2]=summation fx
    fm[3] = forcesin(fm[0], ft[0]) + forcesin(fm[1], ft[1]); //fm[3]= summation fy
    alert("sum fx=" + fm[2]);
    alert("sum fy=" + fm[3]);
    fm[4] = Math.sqrt(Math.pow(fm[2], 2) + Math.pow(fm[3], 2)); //fm[4] = final reultant magnitude
    ft[2] = Math.atan2(fm[3], fm[2]);
    ft[2] = (ft[2] * 180) / (Math.PI); //ft[2] = final reultant angle
    ans.innerHTML = "<br> Resultant force : " + fm[4].toString();
    ans.innerHTML += "<br> Resultant angle : " + ft[2].toString();
}
//# sourceMappingURL=app1.js.map