# Minor Losses

<!-- AIM -->

> ## **<span style="color:blue">Aim</span>**

 To Study Minor Losses for the flow of fluid through pipe.

<!-- theory -->
> ## **<span style="color:blue">Theory</span>**

When the fluid is flown through the pipe system some of the energy is lost in overcoming the hydaraulic resistance.The 
local resistance resulting from flow disturbance which are essentially due to change in velocity is called as minor pipe loss.

The minor pipe loss includes the following cases:

1.Loss of head due to sudden enlargement

2.Loss of head due to sudden contraction

3.Loss of head at entrance of pipe

4.Loss of head at exit of pipe

5.Loss of head due to bend in pipe

6.Loss of head due to obstruction in pipe

7.Loss of head due to fitting in pipe
           
<u> **<span style="color:yellow"> 1. Loss due to sudden enlargement: </span>** </u>

Consider, a liquid flowing through a pipe having sudden enlargement.The flow is decreased abruptly and eddy's are formed
causing loss  of energy.

Consider two sections, 1-1 before enlargement and 2-2 after enlargement.

A<sub>1</sub>= Area of pipe at cross section 1-1
           
p<sub>1</sub>= Pressure at section 1-1
           
v<sub>1</sub>= Velocity at section 1-1
           
Similarly,
A<sub>2</sub>= Area of pipe at cross section 2-2

p<sub>2</sub>= Pressure at section 2-2

v<sub>2</sub>= Velocity at section 2-2

p<sub>0</sub>= Intensity of pressure of liquid

h<sub>L</sub> = Loss of head due to sudden enlargement
           
&rho;<sub>g</sub>= w = Specific weight of water
           
Since there is no sudden change in cross section area of the flow passage, the liquid emerging from this small pipe is not 
able to follow the abrupt change of boundary, consequently at this section, the flow passage separates at boundary forming a region in which turbulent eddy's are formed which result in loss of energy which separation extends upto some distance on the downstream where this flowing liquid again follows the boundary of pipe of large cross section. The section 1-1 and 2-2 are located in such way that velocities are practically assumed to be uniform from continuity equation.
        
A<sub>1</sub>v<sub>1</sub>-A<sub>2</sub>v<sub>2</sub>=0

Q=A<sub>1</sub>v<sub>1</sub>=A<sub>2</sub>v<sub>2</sub>

As v<sub>2</sub>&lt;v<sub>1</sub> change of momentum takes place as the liquid flows from narrow to wide pipe.
The change of momentum/sec is also equal to the net force acting in direction of flow on the liquid between secion 1-1 and 2-2.


p<sub>1</sub>A<sub>1</sub> + p(A<sub>2</sub>-A<sub>1</sub>) - p<sub>2</sub>A<sub>2</sub> = 0


It is experimentally found that p = p<sub>1</sub>, the net force (p<sub>2</sub>A<sub>2</sub>)p<sub>1</sub>. The net momentum of liquid  passing through 1-1.


mv = (w/g)*(Qv<sub>1</sub>)
           
At section 2-2,
mv = (w/g)*(Qv<sub>2</sub>)

Change of momentum = (w/g)*Q(v<sub>1</sub>-v<sub>2</sub>)

(p<sub>2</sub>-p<sub>1</sub>)A<sub>2</sub>=(w/g)*Q(v<sub>2</sub>-v<sub>1</sub>)

((p<sub>2</sub>-p<sub>1</sub>)A<sub>2</sub>)/w =(Q/g)*(v<sub>2</sub>-v<sub>1</sub>)

(p<sub>2</sub>-p<sub>1</sub>)/w = (v<sub>2</sub>/g)*(v<sub>2</sub>-v<sub>1</sub>)

Now, if here is loss of head between the section 1-1 and 2-2.

Then due to sudden enlargement and using equation of Bernoulli's theorem in section 1-1 and 2-2

(p<sub>1</sub>/w)+Z<sub>1</sub>+(v<sub>1</sub><sup>2</sup>/2g)=(p<sub>2</sub>/w)+Z<sub>2</sub>+(v<sub>2</sub><sup>2</sup>/2g)+h<sub>L</sub>

But Z<sub>1</sub>=Z<sub>2</sub>

h<sub>exp</sub>=((p<sub>1</sub>-p<sub>2</sub>)/w)+((v<sub>1</sub><sup>2</sup>-v<sub>2</sub><sup>2</sup>)/2g)

Substituting values of (p<sub>1</sub>-p<sub>2</sub>)/w=(v<sub>2</sub>/g)*(v<sub>2</sub>-v<sub>1</sub>) ,we get

h<sub>exp</sub>=((v2/g)*(v<sub>2</sub>-v<sub>1</sub>))+(v<sub>1</sub><sup>2</sup>-v<sub>2</sub><sup>2</sup>)/2g

h<sub>exp</sub>=(1/2g)*(2v<sub>1</sub><sup>2</sup>-2v<sub>1</sub>v<sub>2</sub>+v<sub>1</sub><sup>2</sup>-v<sub>2</sub><sup>2</sup>)

h<sub>exp</sub>=(1/g)*(v<sub>1</sub><sup>2</sup>-v<sub>2</sub><sup>2</sup>)
as h<sub>exp</sub>=((v<sub>1</sub><sup>2</sup>)/2g)*(1-(A<sub>1</sub>/
A<sub>2</sub>))<sup>2</sup>=((v<sub>2</sub><sup>2</sup>)/2g)*((A<sub>2</sub>/A<sub>1</sub>)-1)<sup>2</sup>

h<sub>exp</sub>=R<sub>exp</sub>(v<sub>1</sub><sup>2</sup>/2g)
....where R<sub>exp</sub>=(1-(A<sub>1</sub>/A<sub>2</sub>))<sup>2</sup>

<u> **<span style="color:red"> Eddy Loss: </span>** </u>

The expansion loss is expanded exclusively due to eddy formation and continuous sustainance or rotational motion of fluid
           masses.

<u> **<span style="color:red"> Shock Loss: </span>** </u>

Because of abrupt loss due to sudden slowing down of fluid stream. A shock loss is equivalent to destruction in Kinetic Energy
           of their relative motion.A shock loss manifests when fast flowing liquid impinges on a slow flowing or stationary fluids.


<u> **<span style="color:red"> Diffuser: </span>** </u>

A diffuser is a flow passage with a gradual increase in area and pressure rise in  the direction of flow. The reduction in energy
           loss occurs due to elimination of eddies.
           

<u> **<span style="color:yellow"> **2.Loss due to sudden contraction:** </span>** </u>

A converging pipe flow where in stream of flow leaves the boundary at corner of junction and attains a minimum cross section at
           vena contracta. At vena contracta the effective flow area becomes considerably less than the cross section area of small diameter
           of pipe.The accelearting flow in a stable boundary that has no chance to separate and consequently there is loss of head between entrance and
           vena contracta.

A diverging flow downstreams vena contracta where the stream expands and ultimately assume uniform flow over entire cross 
           section area of narrow pipe.

The loss of head in pipe contraction is thus caused by turbulence by abrupt expansion of flow just after it has passed through
           vena contracta and can be approximately equal to loss of heat due to sudden expansion quality.

h<sub>con</sub>=(v<sub>p</sub>-v<sub>2</sub>)<sup>2</sup>/2g

h<sub>con</sub>=R<sub>con</sub>*(v<sub>2</sub><sup>2</sup>/2g)

.....where R<sub>con</sub>=coefficient of contraction

.....take R<sub>con</sub>=0.5 if not specified
           

<u> **<span style="color:yellow">**3.Head loss due to obstruction in flow passage:** </span>** </u>

The loss of energy due to obstruction in pipe takes place on account of reduction in cross section of pipe by presence of
           obstruction which is followed by abrupt enlargement of stream beyond obstruction such as loss of head due to obstruction may be
           computed by applying Bernoulli's theorem. This loss can be computed by applying equation for flow of head due to sudden
           enlargement.

h<sub>exp</sub>=(1/2s)*(v<sub>1</sub>-v<sub>2</sub>)<sup>2</sup>

Consider a pipe of cross section area 'A' and let an obstruction of maximum cross section 'a' be placed in pipe. Thus at section area of flow
           of passage reduces as the liquid flows through pipe, a vena contracta is formed beyond 1-1, after which the stream widens again,between
           vena contracta and section 2-2 at a certain distance away from the obstruction on downstream where the obstruction velocity has 
           again become almost uniform.The flow pattern is Similar to that after sudden enlargement.

Let v<sub>c</sub>=velocity at vena contracta

v=velocity at 2-2

Loss of head due to obstruction in pipe:

h<sub>obs</sub>=[(1/(C<sub>c</sub>(A-a)))-1]<sup>2</sup>*(v<sup>2</sup>/2g)
           

<u> **<span style="color:yellow">**4.Loss of head due to gradual contraction or enlargement:** </span>** </u>

In gradual contraction or enlargement the velocity of fluid gradually increases or decreases beacuse of eddies which are 
           responsible for the dissipation of energy which is eliminated or reduced. Generally loss of head due to gradual contraction or enlargement
           is expressed as

h<sub>grad</sub>=R(v<sub>1</sub>-v<sub>2</sub>)<sup>2</sup>/2g

v<sub>1</sub>=mean velocity at inlet

v<sub>2</sub>=mean velocity at outlet

R=coefficient of gradual contraction and expansion
           In general, value of R depends upon the angle of convergence or divergence and on the ratio of upstream and downstream of area for gradual
           contraction the value of R is usually very small that it may be neglected but for gradual enlargement value of R depends on 
           angle of divergence.
           

<u> **<span style="color:yellow"> **5.Loss of head in bends:** </span>** </u>

The bends are provided in a flow of passage to change the direction of flow. The change in dimension of flow the loss of energy is due to section of flow 
           from boundary and the consequent formation of eddies resulting in dissipation. Formation of energy in turbulence in general of head in bends provided 
           in pipe expressed as

h<sub>bend</sub>=Rv<sup>2</sup>/2g

....where R = coefficient of loss of head in bends, v=mean velocity of flow
           
**Specifications:**

1/2 Horse power pump to circulate water through the pipe.
           Sump tank and measuring tank of suitable capacity.
           Fitting comprising of elbows, sudden contraction, bus cap, plug and floor plunge.
           Valve comprising of Globe valve, diaphragm valve, valve check, ball valve, butterfly valve, needle valve, angle valve,etc.
           
> ## **<span style="color:blue">Procedure</span>**
           
Before starting experiment, see that bypass valve ,flow control valve and manometer base cocks are fully opened.

1.Fill up sufficient clean water in sump tank.
           
2.Start the pump and close the bypass valve so that water starts flowing through the manometer overflow tubes.This ensures that
           there is no air gap in the manometer piping.
           
3.Now open the bypass valve by few turns such that the pressure in the piping is decreased.
           
4.Select any one valve or fitting for testing of head loss.
           
5.Start the water flow only through that valve or fitting.
           
6.Note down the manometer difference and time required for 10 cm rise in water in the measuring tank.
           
7.Repeat the above procedure for different flow rates.



